Database adapter for MongoDB
=====================

Uses [Mongoose ODM](http://mongoosejs.com/) module.

**Docs:**

* MongoBD: http://docs.mongodb.org/manual
* Mongoose: http://mongoosejs.com/docs/guide.html

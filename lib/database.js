/**
 * Project: database-adapter
 *
 * Adapter for MongoDB
 *
 * Author: Anton Petrov
 * Date: October 2014
 *
 */

/**
 * Documentation:
 *
 * 
 */

var config = require('config-adapter');
var logger = require('logger-adapter');
var mongoose = require('mongoose');

module.exports = new Database();

function Database()
{
	/*************************************/
	/* M O D E L S
	/*************************************/

	var User = require('./models.js').User;
	var Playlist = require('./models.js').Playlist;

	/*************************************/
	/* C O N N E C T I O N
	/*************************************/

	// Config
	mongoose.connect('mongodb://' +
		config.database.host + ':' +
		config.database.port + '/' +
		config.database.name, {
		user: config.database.user,
		pass: config.database.pass
	});

	// Connection instance
	var db = mongoose.connection;


	// Callbacks
	db.on('error', function(err)
	{
	    logger.error('[database-adapter]: Connection error: ' + err.message);
	});

	db.once('open', function()
	{
	    logger.info('[database-adapter]: Connected to DB!');
	});

	/*************************************/
	/* P U B L I C
	/*************************************/

	/**
	 * Function creates new User in DB
	 * @param  {Object}   params contains fields from User schema
	 * @return callbak(err, user)
	 */
	this.createUser = function(params, callback)
	{
		var user = new User({
			name: 		params.name,
			image: 		params.image,
			email: 		params.email,
			password: 	params.password,
			salt: 		params.salt,
			services: 	params.services || []
		});

		user.save(function(err)
		{
        	if (err)
        	{
        		logger.error(err.message);
        		callback(err)
            	return;
        	}

        	logger.info('[database-adapter]:[createUser]: User with _id: ' + user._id + ' created');
        	callback(null, user);
        });
	}

	/**
	 * Function crates new Playlist in DB
	 * @param  {ObjectId} uid      User ID
	 * @param  {Object}   params   contains fields from Playlist schema
	 * @return callbak(err, playlist)
	 */
	this.createPlaylist = function(uid, params, callback)
	{
		var playlist = new Playlist({
			user: 		     uid,
			name: 			 params.name,
			description: 	 params.description,
			image: 			 params.image,
			shared: 		 params.shared,
			tracks: 		 params.tracks || [],
			enabledServices: params.enabledServices || []
		});

		playlist.save(function(err)
		{
        	if (err)
        	{
        		logger.error('[database-adapter]:[createPlaylist]: ' + err.message);
        		callback(err);
            	return;
        	}

        	logger.info('[database-adapter]:[createPlaylist]: Playlist with _id: ' + playlist._id + ' created');
        	callback(null, playlist);
        });
	}

	this.getUserById = function(uid, callback)
	{
		User.findById(uid).exec(callback);
	}

	this.getUserServiceData = function(uid, serviceType, callback)
	{
		User.findOne({ _id: ObjectId(uid), 'services.type': serviceType }, { 'services.$': 1 }, function(err, result)
		{
			if (err)
			{
				logger.error('[database-adapter]:[getUserServiceData]: ' + err.message);
        		callback(err);
        		return;
			}

			// If not found return null
			if (!result || !result.services || !result.services.length)
			{
				callback(null, null);
				return;
			}

			// Ok, we found the service, get data
			var data = result.services[0].data;

			callback(null, data);
		});
	}

	this.updateUserServiceData = function(uid, serviceType, data, callback)
	{
		User.update({ _id: ObjectId(uid), 'services.type': serviceType }, { '$set': { 'services.$.data': data } }, function(err)
		{
			if (err)
			{
				logger.error('[database-adapter]:[updateUserServiceData]: ' + err.message);
        		callback(err);
        		return;
			}

			callback();
		});
	}

	/*************************************/
	/* S U B R O U T I N E S
	/*************************************/

	var ObjectId = mongoose.Types.ObjectId;
}

/**
 * Project: database-adapter
 *
 * Mongoose schemas and models for MongoDB
 *
 * Author: Anton Petrov
 * Date: October 2014
 *
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

function models()
{
	var ObjectId = Schema.Types.ObjectId;

	var serviceTypes= [
		'spotify',
		'vk',
		'groovshark'
	];

	/*************************************/
	/* S C H E M A S
	/*************************************/

	// UserServiceInfo shema
	var UserServiceInfo = new Schema({
		type:   { type: String, enum: serviceTypes, required: true },
		data: {},
	});

	// PlServiceInfo schema
	var PlServiceInfo = new Schema({
		type: 		{ type: String, enum: serviceTypes, required: true },
		playlistId: { type: String, required: true },
		lastSync: 	{ type: Date, required: true },
	});

	// Track schema
	var Track = new Schema({
		artist: 	{ type: String, required: true  },
		name: 		{ type: String, required: true  },
		album: 		{ type: String, required: false },
		duration: 	{ type: String, required: false },
		serviceIds: {}
	});

	// User schema
	var User = new Schema({
		name: 		{ type: String, required: true  },
		image: 		{ type: String, required: false },
		email: 		{ type: String, required: true  },
		password: 	{ type: String, required: true  },
		salt: 		{ type: String, required: true  },
		services: 	[UserServiceInfo]
	});

	// Playlist schema
	var Playlist = new Schema({
		user: 			 { type: ObjectId, required: true  },
		name: 			 { type: String,   required: true  },
		description: 	 { type: String,   required: false },
		image: 			 { type: String,   required: false },
		shared: 		 { type: Boolean,  required: true  },
		tracks: 		 [Track],
		enabledServices: [PlServiceInfo]
	});

	/*************************************/
	/* M O D E L S
	/*************************************/

	this.User = mongoose.model('User', User);

	this.Playlist = mongoose.model('Playlist', Playlist);
}

module.exports = new models();

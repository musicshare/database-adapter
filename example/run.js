var db = require('../lib/database.js');
/*
// Create USER
db.createUser({
	name: 		'Anton Petrov',
	image: 		'http://hsto.org/getpro/habr/avatars/282/ebc/a52/282ebca522b6e7556b7c60183818ac40.jpg',
	email: 		'petrov.aap@gmail.com',
	password: 	'kd3401sdSHDd9231nmS3j342Jud',
	salt: 		'jSsndja9d8912jsad10qwdj1',
	services: 	[{
		type: 'spotify',
		data: {
			token: {
				access_token: 'BQASCUswRLRIO6uvQ9OzvmoVq4X6n9F7Vavgq6NsNo7ycFZepN6YYijW9eVirsXRvAVLeEoDM2Yd9nwyLq6EsVq0TKyEK1dcHsIEo8PJZU6yNb3Iyf0vgAGawK4jH7y04YwrCDR3qMXS2FfWKEwq_AD70ig4fb7JJ4h1ZzSixA9SphTcGa6iFyC6TbV-puc2SZCl35tDpKdbRphxIxl3teSW-OoWc8CMCmF7jBbRo3tp80iVPoMCAJ8mFH69',
				refresh_token: 'AQCRRTE8l7Q_6bx1Cpu3LqQPTxXJIJkMBYc2LvSIGJHEXkfsbDHI15KHs76rRfzPyEnb5W80rTWLL2ZNiANIBaWpVV9dvDSXa_KzESNz1QR5snC-01HyJypsM9L7cxrNE2E',
				expires_in: 3600,
				timestamp: 1414441151064,
			}
		}
	}, {
		type: 'vk',
		data: {
			token: {}
		}
	}]
},
function(err, user)
{
	if(err)
	{
		return;
	}

	console.log(user);

	// Create PLAYLIST for USER
	db.createPlaylist(user._id, {
		name: 		 'Starred',
		description: 'My starred songs',
		shared: 	 false,
		tracks: [{
			artist: 'Nirvana',
			name:   'Smells like teens spirit'
		}, {
			artist: 'Green Day',
			name:   'American Idiot'
		}],
		enabledServices: []
	},
	function(err, playlist)
	{
		if(err)
		{
			return;
		}

		console.log(playlist);
	});
});
*/
/*db.getUserById('544ea9e2e2d03e7612b0493b', function(err, user)
{
	if(err)
	{
		return;
	}

	console.log(user);
});
*/

db.getUserServiceData('544fe2b937bc00f61c60d706', 'spotify', function(err, service)
{
	if(err)
	{
		return;
	}

	console.log(service);
});

db.updateUserServiceData('544fe2b937bc00f61c60d706', 'spotify', { token: { access_token: 123 } },function(err, service)
{
	if(err)
	{
		return;
	}

	console.log('OK!');
});


